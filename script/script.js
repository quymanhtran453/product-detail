$(document).ready(function () {
  const firstImgSwiper = $(".img-swiper-wrapper:first");
  if (firstImgSwiper) {
    firstImgSwiper.addClass("active");
    const image = firstImgSwiper.find("img.swiper-img");
    if (image) {
      const clickedImgSrc = image.attr("src");
      $("#active-bigger-img").attr("src", clickedImgSrc);
    }
  }
});

$(document).ready(function () {
  $("#prev-swiper").on("click", function () {
    const swiperContainer = $(".swiper-container");
    swiperContainer.animate(
      {
        scrollTop: swiperContainer.scrollTop() - 120,
      },
      500
    );
  });
});

$(document).ready(function () {
  $("#next-swiper").on("click", function () {
    const swiperContainer = $(".swiper-container");
    swiperContainer.animate(
      {
        scrollTop: swiperContainer.scrollTop() + 120,
      },
      300
    );
  });
});

$(document).ready(function () {
  $(".img-swiper-wrapper").on("click", function () {
    const image = $(this).find("img.swiper-img");
    if (image) {
      const clickedImgSrc = image.attr("src");
      $("#active-bigger-img").attr("src", clickedImgSrc);
    }
    const imgActive = $(".img-swiper-wrapper.active");
    if (imgActive) {
      imgActive.removeClass("active");
      $(this).addClass("active");
    }
  });
});

$(document).ready(function () {
  $(".review-nav-container").on("click", function () {
    $(this).addClass("active");
    $(".question-nav-container").removeClass("active");
    $(".question-wrapper").removeClass("active");
    $(".review-wrapper").addClass("active");
    $(".active-nav-line").removeClass("active");
  });
});

$(document).ready(function () {
  $(".question-nav-container").on("click", function () {
    $(this).addClass("active");
    $(".review-nav-container").removeClass("active");
    $(".review-wrapper").removeClass("active");
    $(".question-wrapper").addClass("active");
    $(".active-nav-line").addClass("active");
  });
});
/* scroll description */
$(document).ready(function () {
  $(".description-link").click(function () {
    $("html, body").animate(
      {
        scrollTop: $(".description-container").offset().top - 100,
      },
      350
    );
  });
});

/* scroll review */
$(document).ready(function () {
  $(".review-header").click(function () {
    if (!$("review-nav-container").hasClass("active")) {
      $(".review-nav-container").addClass("active");
      $(".question-nav-container").removeClass("active");
      $(".question-wrapper").removeClass("active");
      $(".review-wrapper").addClass("active");
      $(".active-nav-line").removeClass("active");
    }
    $("html, body").animate(
      {
        scrollTop: $(".review-wrapper").offset().top - 120,
      },
      350
    );
  });
});

/* question scroll */
$(document).ready(function () {
  $(".product-question").click(function () {
    if (!$(".question-nav-container").hasClass("active")) {
      $(".question-nav-container").addClass("active");
      $(".review-nav-container").removeClass("active");
      $(".review-wrapper").removeClass("active");
      $(".question-wrapper").addClass("active");
      $(".active-nav-line").addClass("active");
    }
    $("html, body").animate(
      {
        scrollTop: $(".question-wrapper").offset().top - 120,
      },
      350
    );
  });
});

/* see more description handle */

$(document).ready(function () {
  const descriptionElement = $(".description-detail");
  if (descriptionElement) {
    if (descriptionElement.height() > 21 * 3) {
      descriptionElement.addClass("shorten");
      $(".see-more").addClass("active");
      $(".description-li").addClass("hidden");
      $(".compound").addClass("hidden");
    }
  }
});

$(document).ready(function () {
  const seeMore = $(".see-more");
  if (seeMore) {
    seeMore.click(function () {
      $(".description-detail").removeClass("shorten");
      $(".description-li").removeClass("hidden");
      $(".compound").removeClass("hidden");
      $(this).removeClass("active");
    });
  }
});
